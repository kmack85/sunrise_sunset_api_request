const axios = require('axios');

let coordinates = [];
let requests = [];
let outputs = [];
//[{sunrise: '5:06:46 PM', day_length: '14:27:08'}, {sunrise: '7:35:33 AM', day_length: '12:19:50'}, {sunrise: '5:42:31 AM', day_length: '11:46:32'}, {sunrise: '7:48:13 PM', day_length: '12:58:38'}, {sunrise: '7:44:42 PM', day_length: '12:26:50'}];

//generate random numbers within a specified range
function generateRandom(min, max) {
    return (Math.random() * (max - min) + min).toFixed(7);
}

//populate coordinates array
function populateCoordinates() {
    let counter = 0;
    while (counter < 100) {
        let longitude = generateRandom(-180, 180);
        let latitude = generateRandom(-90, 90);
        coordinates.push({latitude, longitude });
        counter++;
    };
}

populateCoordinates();
//console.log(coordinates);

//create get requests for lat/long
function generateRequests() {
    for (let i=0; i < coordinates.length; i++){
        let current = coordinates[i];
        let request = `https://api.sunrise-sunset.org/json?lat=${current.latitude}&lng=${current.longitude}`;
        requests.push(request);
    };
}

generateRequests();
//console.log(requests);

//perform get request function
function getRequest(request){
    return axios.get(request)
    .then(response => {
            let result = [];
            let sunrise = response.data.results.sunrise;
            let dayLength = response.data.results.day_length;
            result.push({sunrise, dayLength});
            console.log(result);
        })
    .catch(error => {
            console.log(error);
        })   
}

//getRequest('https://api.sunrise-sunset.org/json?lat=36.7201600&lng=-4.4203400');
getRequest(requests[0]);
//loop through requests and call getRequest on each

/* for (let i = 0; i < 10; i++){
     getRequest(requests[i])
}; 
*/

//convert 12hour time to 24hour time for easier comparison
function convertTime12to24(time12h) {
    const [time, modifier] = time12h.split(' ');
    let [hours, minutes, seconds] = time.split(':');
    if (hours === '12') {
        hours = '00';
    }
    if (modifier === 'PM') {
        hours = parseInt(hours, 10) + 12;
    }
    return `${hours}:${minutes}:${seconds}`;
    };

//console.log(convertTime12to24(outputs[0].sunrise));

//convert 24hour time to date object to allow comparison   
function strToDate(timeStr){
    let [hours, minutes, seconds] = timeStr.split(':');
    let timeObject = new Date();
    timeObject.setHours(hours,minutes,seconds);
    return timeObject;
};

//find earliest sunrise and return day length
function earliestDayLength(){
let earliestTime = strToDate(convertTime12to24(outputs[0].sunrise));
let dayLength = '';
for (let i = 0; i < outputs.length; i++) {
    let currentObject = strToDate(convertTime12to24(outputs[i].sunrise));
    if(currentObject < earliestTime) {
        earliestTime = currentObject;
        dayLength = outputs[i].day_length;
    };
  };
  return dayLength;
}

//console.log(earliestDayLength());
