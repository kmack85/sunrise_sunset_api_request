Ojectives:

Using https://sunrise-sunset.org/api, fetch the sunrise / sunset times
Generate a list of 100 random lat / long points around the world
Run 5 requests in parrallel, run next 5 after 5 second break if they've all finished
Find earliest sunrise and list the day length for this time

Bonus points:
Useramda, implement some functional programming
Use ES6/7 array features•Handle errors properly
Additional functionality
Unit tests

Current Code Status – Working Units:

1. Generate random latitude and longitude values

2. Use latitude and longitude values to populate ‘get requests’

3. Create a for loop to iterate through requests object and call getRequest function on each request. Currently, for loop is set to 5 requests to prevent connection resfusal by api

4. Function to convert sunrise time to 24hour time

5. Function to convert 24hour time into a date object

6. Function to loop through output objects to find earliest sunrise time and return the day length. **I have manually populated the outputs variable in order to complete the program**.

Current Code Status – Not Yet Working Units:

1. After every 5 requests pause for 5 seconds and then continue loop

2. Store output of getRequest function as an object ‘output’.

In order to develop these last two elements, I would need more time to get a solid understanding of asynchronous programming in Javascript; I believe that in order to create the pause I would need to use setTimeout() but I have been unsuccessful thus far. I would then work to convert the working program into Typescript. Once I had the working program in Typescript I would look to work in the functional programming elements of Ramda and complete any necessary refactoring. 